package Professor;

import Model.Profesor.Information.Information;
import Model.Profesor.Professor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProfessorTest {

    @Test
    void testConstructorProfessor() {

        Professor professor = new Professor("Paulina", "Mitrea", "Database");
        assertEquals("Mitrea", professor.getLastName());
        assertEquals("Paulina", professor.getFirstName());
        assertEquals("Database", professor.getMaterials());
    }

    @Test
    void testSetProfessor() {

        Professor professor = new Professor("Paulina", "Mitrea", "Database");
        professor.setLastName("Pop");
        assertEquals("Pop", professor.getLastName());
        professor.setFirstName("Vasile");
        assertEquals("Vasile", professor.getFirstName());
        professor.setMaterials("Algebra");
        assertEquals("Algebra", professor.getMaterials());
    }

    @Test
    void testVerifyId() {

        Professor professor = new Professor("Paulina", "Mitrea", "Database");
        assertEquals(1, professor.verifyId("mitrea.paulina"));
    }

    @Test
    void testVerifyPassword() {

        Professor professor = new Professor("Paulina", "Mitrea", "Database");
        assertEquals(1, professor.verifyPassword("pass1"));
    }

    @Test
    void testStudentList() {

        Information information = new Information("Paulina", "Mitrea", "Database");
        assertEquals("Student year 1 - 1", information.studentsList(1, 1));
    }

}
