package Student;

import Model.Student.SchoolSituation.SchoolSituation;
import Model.Student.Student;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentTest {

    @Test
    void testConstructorStudent() {

        Student student = new Student("Anca", "Lombrea", 2);
        assertEquals("Lombrea", student.getLastName());
        assertEquals("Anca", student.getFirstName());
        assertEquals(2, student.getStudyYear());
    }

    @Test
    void testSetStudent() {

        Student student = new Student("Anca", "Lombrea", 2);
        student.setLastName("Popescu");
        assertEquals("Popescu", student.getLastName());
        student.setFirstName("Emilia");
        assertEquals("Emilia", student.getFirstName());
        student.setStudyYear(3);
        assertEquals(3, student.getStudyYear());
    }

    @Test
    void testVerifyId() {

        Student student = new Student("Anca", "Lombrea", 2);
        assertEquals(3, student.verifyId("lombrea.anca"));
    }

    @Test
    void testVerifyPassword() {

        Student student = new Student("Anca", "Lombrea", 2);
        assertEquals(1, student.verifyPassword("pass1"));
    }

    @Test
    void testPerformanceScholarship() {

        SchoolSituation situtation = new SchoolSituation("Anca", "Lombrea", 2);
        assertEquals("You get the performance scholarship!", situtation.performanceScholarship((float) 9.3));
        assertEquals("You do not get the performance scholarship!", situtation.performanceScholarship((float) 8.2));
    }

    @Test
    void testScholarship() {

        SchoolSituation situtation = new SchoolSituation("Anca", "Lombrea", 2);
        assertEquals("You get the scholarship!", situtation.scholarship((float) 8.9, (float) 8.7));
        assertEquals("You do not get the scholarship!", situtation.scholarship((float) 6.3, (float) 8.7));
    }

}
