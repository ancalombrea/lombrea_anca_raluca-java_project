package Model.Student.SchoolSituation;

import java.util.Scanner;

public class Scholarship {

    public float socialScholarshipSem1(int nrMembers) {

        float june, july, august;
        float total;
        float value;
        System.out.println("Net income for June for the whole family:");
        Scanner scanner = new Scanner(System.in);
        june = scanner.nextFloat();
        System.out.println("Net income for July for the whole family:");
        july = scanner.nextFloat();
        System.out.println("Net income for August for the whole family:");
        august = scanner.nextFloat();
        total = june + july + august;

        value = total / nrMembers / 3;
        return value;
    }

    public float socialScholarshipSem2(int nrMembers) {

        float october, november, december;
        float total;
        float value;
        System.out.println("Net income for October for the whole family:");
        Scanner scanner = new Scanner(System.in);
        october = scanner.nextFloat();
        System.out.println("Net income for November for the whole family:");
        november = scanner.nextFloat();
        System.out.println("Net income for December for the whole family:");
        december = scanner.nextFloat();
        total = october + november + december;

        value = total / nrMembers / 3;
        return value;
    }

    public boolean performanceScholarship(float average) {

        return (average > 9);
    }

    public boolean scholarship(int year, float arithmeticalAverage, float weightedAverage) {

        switch (year) {
            case 1: {
                return (weightedAverage > 8.57 && arithmeticalAverage > 8.46);
            }
            case 2: {
                return (weightedAverage > 7.85 && arithmeticalAverage > 7.9);
            }
            case 3: {
                return (weightedAverage > 7.27 && arithmeticalAverage > 7.57);
            }
            default:
                return false;
        }
    }
}
