package Model.Student.SchoolSituation;

public class WeightedAverage extends Catalog {

    private float average = 0;

    public float average(int year) throws SumException {

        switch (year) {
            case 1: {
                int sum = yearOneSumWithCredits();
                average = (float) sum / 60;
                System.out.println(average);
                if (average > 10) {

                    throw new SumException("Wrong average!");
                }
            }
            break;
            case 2: {
                int sum = yearTwoSumWithCredits();
                average = (float) sum / 60;
                System.out.println(average);
                if (average > 10) {

                    throw new SumException("Wrong average!");
                }
            }
            break;
            case 3: {
                int sum = yearThreeSumWithCredits();
                average = (float) sum / 60;
                System.out.println(average);
                if (average > 10) {

                    throw new SumException("Wrong average!");
                }
            }
        }
        return average;
    }
}
