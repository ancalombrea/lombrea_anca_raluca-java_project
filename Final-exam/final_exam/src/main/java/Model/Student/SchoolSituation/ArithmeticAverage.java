package Model.Student.SchoolSituation;

public class ArithmeticAverage extends Catalog {

    private int sum = 0;
    private float average = 0;

    public float average(int year) throws SumException {

        switch (year) {
            case 1: {
                for (YearOne material : YearOne.values()) {
                    System.out.println("Get note to: ");
                    System.out.println(material);
                    setNote(1);
                    System.out.println(getNote());

                    sum = sum + getNote();
                }
                average = (float) sum / 15;
                if (average > 10) {

                    throw new SumException("Wrong average!");
                }
                System.out.println(average);
            }
            break;

            case 2: {
                for (YearTwo material : YearTwo.values()) {
                    System.out.println("Get note to: ");
                    System.out.println(material);
                    setNote(1);
                    System.out.println(getNote());

                    sum = sum + getNote();
                }
                average = (float) sum / 15;
                if (average > 10) {

                    throw new SumException("Wrong average!");
                }
                System.out.println(average);
            }
            break;

            case 3: {
                for (YearThree material : YearThree.values()) {
                    System.out.println("Get note to: ");
                    System.out.println(material);
                    setNote(1);
                    System.out.println(getNote());

                    sum = sum + getNote();
                }
                average = (float) sum / 16;
                if (average > 10) {

                    throw new SumException("Wrong average!");
                }
                System.out.println(average);
            }
            break;
        }
        return average;
    }

}

