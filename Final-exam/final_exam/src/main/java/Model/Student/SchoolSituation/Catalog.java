package Model.Student.SchoolSituation;

import java.util.Scanner;

public class Catalog {

    private int note;
    private int credit;
    private int sum = 0;

    public int yearOneSumWithCredits() {

        for (YearOne material : YearOne.values()) {
            if (material == YearOne.ANALYSIS || material == YearOne.COMPUTER_ARCHITECTURE ||
                    material == YearOne.ELECTRONIC_CIRCUITS || material == YearOne.PROGRAMMING ||
                    material == YearOne.ANALYSIS_2 || material == YearOne.SPECIAL_MATHEMATICS ||
                    material == YearOne.ELECTRICAL || material == YearOne.GRAPHICS) {
                setCredit(5);
            }
            if (material == YearOne.ALGEBRA || material == YearOne.PHYSICS) {
                setCredit(4);
            }
            if (material == YearOne.INFORMATICS || material == YearOne.CHEMISTRY) {
                setCredit(3);
            }
            if (material == YearOne.ENGLISH || material == YearOne.SPORT || material == YearOne.ENGLISH_2) {
                setCredit(2);
            }

            System.out.println("Get note to: ");
            System.out.println(material);
            setNote(1);
            System.out.println(getNote());

            sum = sum + getNote() * getCredit();
        }
        return sum;
    }

    public int yearTwoSumWithCredits() {

        for (YearTwo material : YearTwo.values()) {
            if (material == YearTwo.ELECTRONICS || material == YearTwo.PROGRAMMING ||
                    material == YearTwo.SYSTEMS_THEORY || material == YearTwo.ALGORITHM_DESIGN ||
                    material == YearTwo.DATABASE || material == YearTwo.MICROPROCESSORS) {
                setCredit(5);
            }
            if (material == YearTwo.NUMERICAL_CALCULATION || material == YearTwo.MEASUREMENTS ||
                    material == YearTwo.MODELING_PROCESSES || material == YearTwo.NUMERICAL_DEVICES ||
                    material == YearTwo.MECHANICS || material == YearTwo.SIGNALS) {
                setCredit(4);
            }
            if (material == YearTwo.ENGLISH || material == YearTwo.ENGLISH_2 ||
                    material == YearTwo.SPORT) {
                setCredit(2);
            }

            System.out.println("Get note to: ");
            System.out.println(material);
            setNote(1);
            System.out.println(getNote());

            sum = sum + getNote() * getCredit();
        }
        return sum;
    }

    public int yearThreeSumWithCredits() {

        for (YearThree material : YearThree.values()) {
            if (material == YearThree.PRACTICE || material == YearThree.TS) {
                setCredit(6);
            }
            if (material == YearThree.IRA || material == YearThree.SED ||
                    material == YearThree.IS || material == YearThree.EP) {
                setCredit(5);
            }
            if (material == YearThree.IRA2 || material == YearThree.STR ||
                    material == YearThree.PRACTICE2) {
                setCredit(4);
            }
            if (material == YearThree.II || material == YearThree.EAEE ||
                    material == YearThree.EAHP || material == YearThree.TD) {
                setCredit(3);
            }
            if (material == YearThree.LE || material == YearThree.MANAGEMENT) {
                setCredit(2);
            }

            System.out.println("Get note to: ");
            System.out.println(material);
            setNote(1);
            System.out.println(getNote());

            sum = sum + getNote() * getCredit();
        }
        return sum;
    }

    public int getNote() {
        return note;
    }

    public int getCredit() {
        return credit;
    }

    public void setNote(int note) {
        Scanner s = new Scanner(System.in);
        note = s.nextInt();
        this.note = note;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }
}
