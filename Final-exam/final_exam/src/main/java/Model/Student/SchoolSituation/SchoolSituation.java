package Model.Student.SchoolSituation;

import Model.Student.Student;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class SchoolSituation extends Student {

    public SchoolSituation(String firstName, String lastName, int year) {

        super(firstName, lastName, year);
    }

    public float arithmeticalAverage() {
        float average = 0;
        int nr = getStudyYear();
        System.out.println("The year of study is: " + nr);

        ArithmeticAverage a = new ArithmeticAverage();
        try {

            average = a.average(nr);

        } catch (SumException exception) {
            System.out.println("Too high average!");
        }
        return average;
    }

    public float weightedAverage() {
        float average = 0;
        int nr = getStudyYear();
        System.out.println("The year of study is: " + nr);

        WeightedAverage b = new WeightedAverage();
        try {

            average = b.average(nr);

        } catch (SumException exception) {
            System.out.println("Too high average!");
        }
        return average;
    }

    public float socialScholarshipSemester1() {

        int numberOfMembers;
        float valueOf;
        System.out.println("Number of family members: ");
        Scanner scanner = new Scanner(System.in);
        numberOfMembers = scanner.nextInt();

        Scholarship social = new Scholarship();
        valueOf = social.socialScholarshipSem1(numberOfMembers);
        if (valueOf > 1346) {
            System.out.println("You do not get social scholarship");
        } else {
            System.out.println("You get social scholarship");
        }
        return valueOf;
    }

    public float socialScholarshipSemester2() {

        int nrOfMembers;
        float valueOf;
        System.out.println("Number of family members: ");
        Scanner scanner = new Scanner(System.in);
        nrOfMembers = scanner.nextInt();

        Scholarship social = new Scholarship();
        valueOf = social.socialScholarshipSem2(nrOfMembers);
        if (valueOf > 1346) {
            System.out.println("You do not get social scholarship");
        } else {
            System.out.println("You get social scholarship");
        }
        return valueOf;
    }

    public String performanceScholarship(float weightedAverage) {

        String trueCondition = "You get the performance scholarship!";
        String falseCondition = "You do not get the performance scholarship!";
        Scholarship performance = new Scholarship();
        boolean average;
        average = performance.performanceScholarship(weightedAverage);
        if (average) {
            return trueCondition;
        } else {
            return falseCondition;
        }
    }

    public String scholarship(float weightedAverage, float arithmeticalAverage) {

        int nr = getStudyYear();
        System.out.println("The year of study is: " + nr);
        String trueCondition = "You get the scholarship!";
        String falseCondition = "You do not get the scholarship!";
        Scholarship scholarship = new Scholarship();
        boolean average;
        average = scholarship.scholarship(nr, arithmeticalAverage, weightedAverage);
        if (average) {
            return trueCondition;
        } else {
            return falseCondition;
        }
    }

    public void printPersonalData() {

        int numberOfMembers;
        float valueOf;

        try {
            FileWriter myWriter = new FileWriter("src/main/java/Model/Student/SchoolSituation/PersonalData");
            myWriter.write("Last name: " + getLastName());
            myWriter.write("\nFirst name: " + getFirstName());
            myWriter.write("\nYear of study: " + getStudyYear());

            System.out.println("Number of family members: ");
            Scanner scanner = new Scanner(System.in);
            numberOfMembers = scanner.nextInt();
            Scholarship scholarship = new Scholarship();

            valueOf = scholarship.socialScholarshipSem1(numberOfMembers);
            if (valueOf < 1346) {
                myWriter.write("\nYou get the social scholarship of first semester!");
            } else {
                myWriter.write("\nYou do not get the social scholarship of first semester!");
            }
            valueOf = scholarship.socialScholarshipSem2(numberOfMembers);
            if (valueOf < 1346) {
                myWriter.write("\nYou get the social scholarship of second semester!");
            } else {
                myWriter.write("\nYou do not get the social scholarship of second semester!");
            }

            myWriter.close();
            System.out.println("Successfully wrote to the file.");

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

}
