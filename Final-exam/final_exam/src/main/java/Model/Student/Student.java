package Model.Student;

import Model.LogIn;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Student implements LogIn {

    private String firstName;
    private String lastName;
    private int studyYear;
    private int nr, nrPass;

    public Student(String firstName, String lastName, int year) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.studyYear = year;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getStudyYear() {
        return studyYear;
    }

    public void setStudyYear(int studyYear) {
        this.studyYear = studyYear;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", studyYear=" + studyYear +
                '}';
    }

    @Override
    public int verifyId(String name) {
        System.out.println("Enter your id in the following mode: " + modelId);
        try {
            File file = new File("src/main/java/Model/Student/idFile");
            Scanner myReader = new Scanner(file);
            String id;
            int counter = 0;
            nr = 0;
            while (myReader.hasNextLine()) {
                counter++;
                id = myReader.nextLine();
                //System.out.println(id);
                if (name.equals(id)) {
                    nr = counter;
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return nr;
    }

    @Override
    public int verifyPassword(String name) {
//        System.out.println("Enter your password in the following mode: " + modelPassword);
        try {
            File file = new File("src/main/java/Model/Student/passwordFile");
            Scanner myReader = new Scanner(file);
            String id;
            int counter = 0;
            nrPass = 0;
            while (myReader.hasNextLine()) {
                counter++;
                id = myReader.nextLine();
//                System.out.println(id);
                if (name.equals(id)) {
                    nrPass = counter;
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return nrPass;
    }
}
