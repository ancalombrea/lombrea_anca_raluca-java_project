package Model.Profesor.Information;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class StudentsList {

    private final String[] vector = new String[21];

    public String list(int year, int position) throws NumberOfStudentsException {

        switch (year) {
            case 1: {

                try {
                    File file = new File("src/main/java/Model/Profesor/Information/ListYear1");
                    Scanner myReader = new Scanner(file);
                    int i = 1;
                    while (myReader.hasNextLine()) {
                        if (i > 21) {
                            throw new NumberOfStudentsException("Too many students in list!");
                        }
                        vector[i] = myReader.nextLine();
                        i++;
                    }
                    myReader.close();
                } catch (FileNotFoundException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
                return vector[position];
            }
            case 2: {

                try {
                    File file = new File("src/main/java/Model/Profesor/Information/ListYear2");
                    Scanner myReader = new Scanner(file);
                    int i = 1;
                    while (myReader.hasNextLine()) {
                        if (i > 21) {
                            throw new NumberOfStudentsException("Too many students in list!");
                        }
                        vector[i] = myReader.nextLine();
                        i++;
                    }
                    myReader.close();
                } catch (FileNotFoundException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
                return vector[position];
            }
            case 3: {

                try {
                    File file = new File("src/main/java/Model/Profesor/Information/ListYear3");
                    Scanner myReader = new Scanner(file);
                    int i = 1;
                    while (myReader.hasNextLine()) {
                        if (i > 21) {
                            throw new NumberOfStudentsException("Too many students in list!");
                        }
                        vector[i] = myReader.nextLine();
                        i++;
                    }
                    myReader.close();
                } catch (FileNotFoundException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }
                return vector[position];
            }
        }
        return vector[position];
    }
}
