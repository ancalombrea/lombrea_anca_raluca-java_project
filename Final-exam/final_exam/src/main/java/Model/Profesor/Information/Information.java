package Model.Profesor.Information;

import Model.Profesor.Professor;

import java.util.Scanner;

public class Information extends Professor {

    public Information(String firstName, String lastName, String materials) {

        super(firstName, lastName, materials);
    }

    public String studentsList(int year, int counter) {

        StudentsList studentlist = new StudentsList();
        int maxNumberOfStudents = 21;
        String[] vector = new String[maxNumberOfStudents];

        try {

            vector[counter] = studentlist.list(year, counter);
        } catch (NumberOfStudentsException exception) {
            System.out.println("Too many students in list!");
        }

        return vector[counter];
    }

    public float salary() {

        Scanner scanner = new Scanner(System.in);
        int year, hours;
        float money;

        System.out.println("Years of work experience: ");
        year = scanner.nextInt();
        System.out.println("Hours of work per week: ");
        hours = scanner.nextInt();

        money = (float) ((float) year * 0.5 * hours * 50);
        return money;
    }
}
