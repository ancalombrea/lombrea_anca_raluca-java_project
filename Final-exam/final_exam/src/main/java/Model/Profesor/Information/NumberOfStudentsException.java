package Model.Profesor.Information;

public class NumberOfStudentsException extends Exception {

    public NumberOfStudentsException(String msg) {

        super(msg);
    }
}
