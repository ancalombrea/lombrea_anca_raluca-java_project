package Model.Profesor;

import Model.LogIn;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Professor implements LogIn {

    private String firstName;
    private String lastName;
    private String materials;
    private int nr, nrPass;

    public Professor(String firstName, String lastName, String materials) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.materials = materials;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMaterials() {
        return materials;
    }

    public void setMaterials(String materials) {
        this.materials = materials;
    }

    @Override
    public String toString() {
        return "Professor{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", materials=" + materials +
                '}';
    }

    @Override
    public int verifyId(String name) {
        System.out.println("Enter your id in the following mode : " + modelId);
        try {
            File file = new File("src/main/java/Model/Profesor/idFile");
            Scanner myReader = new Scanner(file);
            String id;
            int counter = 0;
            nr = 0;
            while (myReader.hasNextLine()) {
                counter++;
                id = myReader.nextLine();
                System.out.println(id);
                if (name.equals(id)) {
                    nr = counter;
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return nr;
    }

    @Override
    public int verifyPassword(String name) {
        System.out.println("Enter your password in the following mode: " + modelPassword);
        try {
            File file = new File("src/main/java/Model/Profesor/passwordFile");
            Scanner myReader = new Scanner(file);
            String id;
            int counter = 0;
            nrPass = 0;
            while (myReader.hasNextLine()) {
                counter++;
                id = myReader.nextLine();
                System.out.println(id);
                if (name.equals(id)) {
                    nrPass = counter;
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return nrPass;
    }
}
