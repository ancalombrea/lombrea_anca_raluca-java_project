package View;

import javax.swing.*;

public class FirstView extends JFrame {

    private JButton studentButton = new JButton("Login");
    private JButton professorButton = new JButton("Login");

    private JTextField studentIdText = new JTextField("");
    private JTextField professorIdText = new JTextField("");
    private JTextField studentPasswordText = new JTextField("");
    private JTextField professorPasswordText = new JTextField("");

    public FirstView() {
        setTitle("Technical University");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(420, 450);

        JLabel idLabel = new JLabel("     ID");
        idLabel.setBounds(10, 170, 110, 20);
        JLabel passwordLabel = new JLabel("Password");
        passwordLabel.setBounds(10, 230, 110, 20);
        JLabel professorLabel = new JLabel("Professor");
        professorLabel.setBounds(130, 100, 110, 20);
        JLabel studentLabel = new JLabel("Student");
        studentLabel.setBounds(260, 100, 110, 20);

        professorIdText.setBounds(120, 150, 120, 40);
        studentIdText.setBounds(250, 150, 120, 40);
        professorPasswordText.setBounds(120, 230, 120, 40);
        studentPasswordText.setBounds(250, 230, 120, 40);

        professorButton.setBounds(130, 300, 100, 30);
        studentButton.setBounds(260, 300, 100, 30);

        add(idLabel);
        add(passwordLabel);
        add(professorLabel);
        add(studentLabel);
        add(professorIdText);
        add(studentIdText);
        add(professorPasswordText);
        add(studentPasswordText);
        add(professorButton);
        add(studentButton);
        JLabel label = new JLabel("");
        add(label);

        setVisible(true);
    }

    public JButton getStudentButton() {
        return studentButton;
    }

    public void setStudentButton(JButton studentButton) {
        this.studentButton = studentButton;
    }

    public JButton getProfessorButton() {
        return professorButton;
    }

    public void setProfessorButton(JButton professorButton) {
        this.professorButton = professorButton;
    }

    public JTextField getStudentIdText() {
        return studentIdText;
    }

    public void setStudentIdText(JTextField studentIdText) {
        this.studentIdText = studentIdText;
    }

    public JTextField getProfessorIdText() {
        return professorIdText;
    }

    public void setProfessorIdText(JTextField professorIdText) {
        this.professorIdText = professorIdText;
    }

    public JTextField getStudentPasswordText() {
        return studentPasswordText;
    }

    public void setStudentPasswordText(JTextField studentPasswordText) {
        this.studentPasswordText = studentPasswordText;
    }

    public JTextField getProfessorPasswordText() {
        return professorPasswordText;
    }

    public void setProfessorPasswordText(JTextField professorPasswordText) {
        this.professorPasswordText = professorPasswordText;
    }

}
