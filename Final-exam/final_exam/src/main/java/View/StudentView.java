package View;

import javax.swing.*;

public class StudentView extends JFrame {

    private JButton socialScholarshipButton = new JButton("Social Scholarship");
    private JButton scholarshipButton = new JButton("Scholarship");
    private JButton performanceButton = new JButton("Performance Scholarship");
    private JButton printButton = new JButton("Print!");
    private JButton arithmeticalAverageButton = new JButton("Arithmetical Average");
    private JButton weightedAverageButton = new JButton("Weighted Average");

    private JTextArea text = new JTextArea();

    public StudentView() {
        setTitle("Technical University - Students");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);

        JLabel labelText = new JLabel("Choose the information!");
        labelText.setBounds(70, 50, 200, 40);

        arithmeticalAverageButton.setBounds(30, 150, 250, 30);
        weightedAverageButton.setBounds(30, 210, 250, 30);
        socialScholarshipButton.setBounds(30, 270, 250, 30);
        scholarshipButton.setBounds(30, 330, 250, 30);
        performanceButton.setBounds(30, 390, 250, 30);
        printButton.setBounds(30, 450, 250, 30);

        text.setBounds(310, 150, 440, 330);

        add(text);
        add(printButton);
        add(performanceButton);
        add(scholarshipButton);
        add(socialScholarshipButton);
        add(weightedAverageButton);
        add(arithmeticalAverageButton);
        add(labelText);
        JLabel label = new JLabel("");
        add(label);

        setVisible(true);
    }


    public JButton getSocialScholarshipButton() {
        return socialScholarshipButton;
    }

    public void setSocialScholarshipButton(JButton socialScholarshipButton) {
        this.socialScholarshipButton = socialScholarshipButton;
    }

    public JButton getScholarshipButton() {
        return scholarshipButton;
    }

    public void setScholarshipButton(JButton scholarshipButton) {
        this.scholarshipButton = scholarshipButton;
    }

    public JButton getPerformanceButton() {
        return performanceButton;
    }

    public void setPerformanceButton(JButton performanceButton) {
        this.performanceButton = performanceButton;
    }

    public JButton getPrintButton() {
        return printButton;
    }

    public void setPrintButton(JButton printButton) {
        this.printButton = printButton;
    }

    public JButton getArithmeticalAverageButton() {
        return arithmeticalAverageButton;
    }

    public void setArithmeticalAverageButton(JButton arithmeticalAverageButton) {
        this.arithmeticalAverageButton = arithmeticalAverageButton;
    }

    public JButton getWeightedAverageButton() {
        return weightedAverageButton;
    }

    public void setWeightedAverageButton(JButton weightedAverageButton) {
        this.weightedAverageButton = weightedAverageButton;
    }


    public JTextArea getText() {
        return text;
    }

    public void setText(JTextArea text) {
        this.text = text;
    }
}
