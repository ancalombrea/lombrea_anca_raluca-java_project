package View;

import javax.swing.*;

public class ProfessorView extends JFrame {

    private JButton yearOneStudentsButton = new JButton("Students of year one");
    private JButton yearTwoStudentsButton = new JButton("Students of year two");
    private JButton yearThreeStudentsButton = new JButton("Students of year three");
    private JButton salaryButton = new JButton("Salary");

    private JTextArea text = new JTextArea();

    public ProfessorView() {
        setTitle("Technical University - Professors");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);

        JLabel labelText = new JLabel("Choose the information!");
        labelText.setBounds(70, 50, 200, 40);

        yearOneStudentsButton.setBounds(30, 150, 250, 30);
        yearTwoStudentsButton.setBounds(30, 210, 250, 30);
        yearThreeStudentsButton.setBounds(30, 270, 250, 30);
        salaryButton.setBounds(30, 330, 250, 30);

        text.setBounds(310, 150, 440, 330);

        add(text);
        add(salaryButton);
        add(yearThreeStudentsButton);
        add(yearTwoStudentsButton);
        add(yearOneStudentsButton);
        add(labelText);
        JLabel label = new JLabel("");
        add(label);

        setVisible(true);
    }


    public JButton getYearThreeStudentsButton() {
        return yearThreeStudentsButton;
    }

    public void setYearThreeStudentsButton(JButton yearThreeStudentsButton) {
        this.yearThreeStudentsButton = yearThreeStudentsButton;
    }

    public JButton getSalaryButton() {
        return salaryButton;
    }

    public void setSalaryButton(JButton salaryButton) {
        this.salaryButton = salaryButton;
    }

    public JButton getYearOneStudentsButton() {
        return yearOneStudentsButton;
    }

    public void setYearOneStudentsButton(JButton yearOneStudentsButton) {
        this.yearOneStudentsButton = yearOneStudentsButton;
    }

    public JButton getYearTwoStudentsButton() {
        return yearTwoStudentsButton;
    }

    public void setYearTwoStudentsButton(JButton yearTwoStudentsButton) {
        this.yearTwoStudentsButton = yearTwoStudentsButton;
    }

    public JTextArea getText() {
        return text;
    }

    public void setText(JTextArea text) {
        this.text = text;
    }
}
