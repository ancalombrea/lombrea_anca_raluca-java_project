package Controller;

import Model.Profesor.Information.Information;
import Model.Profesor.Professor;
import Model.Student.SchoolSituation.SchoolSituation;
import Model.Student.Student;
import View.FirstView;
import View.ProfessorView;
import View.StudentView;

public class Controller {
    private static final SchoolSituation student = new SchoolSituation("Anca", "Lombrea", 2);
    private static final FirstView firstView = new FirstView();
    private static final Student studentLog = new Student("Anca", "Lombrea", 2);
    private static final Professor professorLog = new Professor("Paulina", "Mitrea", "Database");
    private static final Information professorInformation = new Information("Paulina", "Mitrea", "Database");

    public static void main(String[] args) {

        /// Student button
        firstView.getStudentButton().addActionListener(event -> {

            if ((studentLog.verifyId(firstView.getStudentIdText().getText()) != 0) &&
                    (studentLog.verifyPassword(firstView.getStudentPasswordText().getText()) != 0) &&
                    (studentLog.verifyId(firstView.getStudentIdText().getText()) ==
                            studentLog.verifyPassword(firstView.getStudentPasswordText().getText()))) {

                StudentView studentView = new StudentView();

                /// arithmetical
                studentView.getArithmeticalAverageButton().addActionListener(e -> {

                    studentView.getText().setText(" ");
                    studentView.getText().append("\nArithmetical average= ");
                    studentView.getText().append(Float.toString(student.arithmeticalAverage()));
                });

                /// weighted
                studentView.getWeightedAverageButton().addActionListener(e -> {

                    studentView.getText().setText(" ");
                    studentView.getText().append("\nWeighted average= ");
                    studentView.getText().append(Float.toString(student.weightedAverage()));
                });

                /// social
                studentView.getSocialScholarshipButton().addActionListener(e -> {

                    studentView.getText().setText(" ");
                    studentView.getText().append("\nSocial scholarship: ");

                    if (student.socialScholarshipSemester1() > 1346) {
                        studentView.getText().append("\n   You don't have social scholarship on first semester!");
                    } else {
                        studentView.getText().append("\n   You have social scholarship on first semester!");
                    }

                    if (student.socialScholarshipSemester2() > 1346) {
                        studentView.getText().append("\n   You don't have social scholarship on second semester!");
                    } else {
                        studentView.getText().append("\n   You have social scholarship on second semester!");
                    }
                });

                /// scholarship
                studentView.getScholarshipButton().addActionListener(e -> {

                    studentView.getText().setText(" ");
                    studentView.getText().append("\nScholarship: ");
                    studentView.getText().append(student.scholarship(student.weightedAverage(), student.arithmeticalAverage()));
                });

                /// performance scholarship
                studentView.getPerformanceButton().addActionListener(e -> {

                    studentView.getText().setText(" ");
                    studentView.getText().append("\nPerformance scholarship: ");
                    studentView.getText().append(student.performanceScholarship(student.weightedAverage()));
                });

                /// Print
                studentView.getPrintButton().addActionListener(e -> student.printPersonalData());
            }
            else {
                System.out.println("Wrong password");
            }

        });

        /// Professor button
        firstView.getProfessorButton().addActionListener(event -> {

            if ((professorLog.verifyId(firstView.getProfessorIdText().getText()) != 0) &&
                    (professorLog.verifyPassword(firstView.getProfessorPasswordText().getText()) != 0) &&
                    (professorLog.verifyId(firstView.getProfessorIdText().getText()) ==
                            professorLog.verifyPassword(firstView.getProfessorPasswordText().getText()))) {

                ProfessorView professorView = new ProfessorView();

                /// Students list
                professorView.getYearOneStudentsButton().addActionListener(e -> {

                    professorView.getText().setText(" ");
                    professorView.getText().append("\nStudents list: ");
                    int maxNumberOfStudents = 20;
                    for (int i = 1; i <= maxNumberOfStudents; i++) {
                        professorView.getText().append("\n");
                        professorView.getText().append(professorInformation.studentsList(1, i));
                    }
                });
                professorView.getYearTwoStudentsButton().addActionListener(e -> {

                    professorView.getText().setText(" ");
                    professorView.getText().append("\nStudents list: ");
                    int maxNumberOfStudents = 20;
                    for (int i = 1; i <= maxNumberOfStudents; i++) {
                        professorView.getText().append("\n");
                        professorView.getText().append(professorInformation.studentsList(2, i));
                    }
                });
                professorView.getYearThreeStudentsButton().addActionListener(e -> {

                    professorView.getText().setText(" ");
                    professorView.getText().append("\nStudents list: ");
                    int maxNumberOfStudents = 20;
                    for (int i = 1; i <= maxNumberOfStudents; i++) {
                        professorView.getText().append("\n");
                        professorView.getText().append(professorInformation.studentsList(3, i));
                    }
                });

                /// Salary
                professorView.getSalaryButton().addActionListener(e -> {

                    professorView.getText().setText(" ");
                    professorView.getText().append("\nSalary: ");
                    professorView.getText().append(String.valueOf(professorInformation.salary()));
                });
            }
        });

    }
}
